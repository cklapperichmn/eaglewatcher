"""
Stoppable Threads and Signal Handling functionality.

Classes:
    - StoppableThread: A thread that can be stopped. It has a stop() method that sets a _stop_event threading.Event flag.
Functions:
    - register_signals: This registers a function to kill all 'StoppableThread' instances, as well as 'process to kill' if it was passed in.
"""

import functools
import signal
import threading
import time
import os
import subprocess

class StoppableThread(threading.Thread):
    """
    This is a thread that can be stopped. It has a stop() method that sets a _stop_event threading.Event flag.
    """
    def __init__(self,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        self._target = functools.partial(self._target, self._stop_event)
    def stop(self)->None:
        self._stop_event.set()

    def stopped(self)->bool:
        return self._stop_event.is_set()

"""
SIGILL
The SIGILL signal is sent to a process when it attempts to execute an illegal, malformed, unknown, or privileged instruction.
SIGINT
The SIGINT signal is sent to a process by its controlling terminal when a user wishes to interrupt the process. This is typically initiated by pressing Ctrl-C, but on some systems, the "delete" character or "break" key can be used.[5]
SIGQUIT
The SIGQUIT signal is sent to a process by its controlling terminal when the user requests that the process quit and perform a core dump.
SIGTERM
The SIGTERM signal is sent to a process to request its termination. Unlike the SIGKILL signal, it can be caught and interpreted or ignored by the process. This allows the process to perform nice termination releasing resources and saving state if appropriate. SIGINT is nearly identical to SIGTERM.
SIGTSTP
The SIGTSTP signal is sent to a process by its controlling terminal to request it to stop temporarily. It is commonly initiated by the user pressing Ctrl-Z. Unlike SIGSTOP, the process can register a signal handler for or ignore the signal.
SIGHUP
The SIGHUP signal is sent to a process when its controlling terminal is closed. It was originally designed to notify the process of a serial line drop (a hangup). In modern systems, this signal usually means that the controlling pseudo or virtual terminal has been closed.[3] Many daemons will reload their configuration files and reopen their logfiles instead of exiting when receiving this signal.[4] nohup is a command to make a command ignore the signal.
"""
def register_kill_signals(process_to_kill:subprocess.Popen=None)->None:
    """
    This kills all 'StoppableThread' instances, as well as 'process to kill' if it was passed in.
    :param process_to_kill: The process to kill. This is a subprocess.Popen instance
    This only registers the signal handlers for SIGINT and SIGTERM
    :return: None
    """


    def signal_handler(signal, frame):
        print('Eaglewatcher is stopping all threads. Please wait...')
        for thread in threading.enumerate():
            if isinstance(thread, StoppableThread):
                thread.stop()
        if process_to_kill:
            process_to_kill.terminate()
        print('Exiting.')

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    # signal.signal(signal.SIGQUIT, signal_handler)
    # signal.signal(signal.SIGHUP, signal_handler)

if __name__=='__main__':

    import queue
    def testrun(stop_event):
        while not stop_event.is_set():
            print('running')
            time.sleep(1)
        print('done running thread. Joining.')
    q = queue.Queue()

    # sigkill and sigstop are not catchable

    t = StoppableThread(target=testrun, daemon=False)
    t.start()

    # command = 'streamlit run dash.py -- --logfile sampleparser-1.0.0.log'
    command = 'streamlit run sampledashboard.py'

    import subprocess
    p = None
    # print('starting subprocess')
    # p = subprocess.Popen(command, shell=True)
    # print("subprocess started")
    register_kill_signals(p)
    print('Signals registered')

    #  signal.raise_signal(signal.SIGINT)
    while t.is_alive():
        time.sleep(1)
    print('all threads joined!')