"""
EAGLEWATCHER DASHBOARD
Interactive web dashboard for displaying logs created by Eaglewatcher.
Can be run independently of the main app.

GOALS:
- View which files are being monitored by which threads/parsers, and their status
- view last update for each thread/directory
- view recent error messages
"""

import os

import pandas as pd
import streamlit as st

st.set_page_config(layout="wide", page_title="EagleWatcher Dashboard", page_icon="🦅")
import data_structures
from eaglewatcher.eagle_loggers import read_logfile
from eaglewatcher import eagle_loggers


@st.cache_data
def read_logdata(filepath: str, timestamp: float or int):
    with open(filepath, "r") as f:
        loglines = f.readlines()

    logdata = read_logfile(loglines)
    return logdata


@st.cache_data
def split_logdata_tracebacks_notracebacks(df: pd.DataFrame):
    return (
        df[df["operationtype"] == "TRACEBACK"],
        df[df["operationtype"] != "TRACEBACK"],
    )


@st.cache_data
def filter_logdata(df, show_errors_only, show_summarydata, selected_eventtypes: list):

    if show_errors_only:
        df = df[df["status"] == "ERROR"]

    df = df[df["operationtype"].isin(selected_eventtypes)]

    if show_summarydata:
        df = eagle_loggers.summarize_logdata_dataframe(df)

    # df['source_dir'] = df['filepath'].apply(lambda x: os.path.dirname(x))
    return df


def is_logfile(f: str):
    return f.lower().endswith(".log")


def read_logfile_or_dir(logfile: str):
    timestamp = os.path.getmtime(logfile)
    if os.path.isfile(logfile):
        logdata = read_logdata(logfile, timestamp)
    elif os.path.isdir(logfile):
        data = []
        files = os.listdir(logfile)
        filepaths = [os.path.join(logfile, f) for f in files if is_logfile(f)]
        for f in filepaths:
            logdata = read_logdata(f, timestamp)
            data.append(logdata)

        logdata = data[0]
        logdata.last_updated = max([d.last_updated for d in data])
        logdata.dataframe = pd.concat([d.dataframe for d in data])

    else:
        raise FileNotFoundError(f"File or directory not found: {logfile}")
    return logdata


import click


@click.command()
@click.option("-l", "--logfile", help="File or Directory containing logfiles")
def default_dashboard(logfile: str):
    logdata = read_logfile_or_dir(logfile)
    parser_info = logdata.parserinfo
    st.write(f"## {parser_info['PARSER_NAME']} Dashboard")

    LogDataTab, TracebacksTab, ParserInfoTab = st.tabs(
        ["Log Data", "Tracebacks", "Parser Info"]
    )
    dashboard(
        logfile,
        LogDataTab,
        TracebacksTab,
        ParserInfoTab,
        use_sidebar=True,
        logdata=logdata,
    )


def write_filters_and_filter_data(df: pd.DataFrame) -> pd.DataFrame:
    st.write("## Filters")
    show_errors_only = st.checkbox(label="errors only", value=False)
    show_summarydata = st.checkbox(label="summary data", value=False)

    selected_eventtype_category = st.selectbox(
        label="event types",
        options=["File Processing", "File IO", "All", "None"],
        index=2,
    )

    fileprocessing_events = list(data_structures.ProcessingEventType.__members__.keys())
    fileio_events = list(data_structures.FileEventType.__members__.keys())
    loggingevents = list(data_structures.LoggingEventType.__members__.keys())
    allevents = fileprocessing_events + fileio_events + loggingevents

    eventtype_dict = {
        "None": [],
        "File Processing": fileprocessing_events,
        "File IO": fileio_events,
        "Other": loggingevents,
        "All": allevents,
    }

    default_eventtypes = eventtype_dict[selected_eventtype_category]
    selected_eventtypes = st.multiselect(
        "Select Operation Types", options=allevents, default=default_eventtypes
    )

    df = filter_logdata(df, show_errors_only, show_summarydata, selected_eventtypes)
    return df


def prettify_df(df: pd.DataFrame):
    """
    Rename and reorder columns
        "operationtype"->Event,
        "status",
        "datetime",
        "message",
        "loglevel",
        "filepath"->dropped,
        "fullpath",
    """
    # Reorder the columns. This also serves as informal documentation of what columns exist.
    df.rename(
        columns={"operationtype": "Event"},
        inplace=True,
    )
    df.drop(columns=("filepath"), inplace=True)
    ordered_columns = [
        "Event",
        "status",
        "datetime",
        "message",
        "loglevel",
        "fullpath",
    ]
    correct_column_order = ordered_columns + [
        col for col in df.columns if col not in ordered_columns
    ]
    df = df[correct_column_order]

    df.sort_values(by="datetime", ascending=False, inplace=True)
    return df


def dashboard(
    logfile_or_logdir: str,
    logdata_context=None,
    tracebacks_context=None,
    parserinfo_context=None,
    use_sidebar=False,
    logdata: data_structures.LogData = None,
):
    """
    Allows you to display log data as part of a DIFFERENT streamlit dashboard. You can do this using the 'context' arguments.
    If you pass in a logdata object, new logdata won't be read. otherwise, logdata is read from 'logfile_or_logdir'
    :param logfile: a file or dirpath containing eaglewatcher .log files, or a logdata object containing data that was already read.
    :param logdata_context: a streamlit object with a context manager. This is where the individual log lines & filter will be displayed.
    :param tracebacks_context: a streamlit object with a context manager. This is where the tracebacks will be displayed.
    :param parserinfo_context: a streamlit object with a context manager. This is where the parser info like name and version will be displayed
    :param use_sidebar: if True, the filters will be displayed in the sidebar. CAUTION: all streamlit dashboards use the same sidebar, this will write to yours.
    Otherwise, filters get written into the logdata_context object
    :param logdata: a logdata object. If not provided, it will be read from the logfile or logdir
    Logfile format:
    TARGET_DIRECTORY: /path/to/directory
    2024-02-26 12:49:37,584 | LOGLEVEL |  OPERATIONTYPE | FILEPATHS | EXCEPTION
    """
    if not logdata:
        logdata = read_logfile_or_dir(logfile_or_logdir)

    last_updated = logdata.last_updated
    commonpath = logdata.commonpath
    tracebacks, original_df = split_logdata_tracebacks_notracebacks(logdata.dataframe)

    if tracebacks_context:
        with tracebacks_context:
            for row in tracebacks[::-1].iterrows():
                series = row[1]
                tb_string = " | ".join(
                    [
                        str(x)
                        for x in series.drop(["message", "fullpath", "status"]).values
                    ]
                )
                with st.expander(tb_string):  # drop the 'message' column
                    st.code(series["message"])

    if logdata_context:
        with logdata_context:
            if not use_sidebar:
                filter_context = st.expander("Filters", expanded=True)
            else:
                filter_context = st.sidebar
            with filter_context:
                st.write("#### Last Updated: ", logdata.last_updated)
                df = write_filters_and_filter_data(original_df)

            st.write("#### Log Data")
            st.dataframe(prettify_df(df), hide_index=True)

    if parserinfo_context:
        with parserinfo_context:
            columns = st.columns(2)
            with columns[0]:
                st.write("#### Log Info")
                st.write("**Logfile:** ", logfile_or_logdir)
                st.write("**Last Updated:** ", last_updated)
                st.write("**Target Directories:**", logdata.target_directories)
                st.write("**Base Path:**", commonpath)

            with columns[1]:
                st.write("#### Parser Info")
                st.write("**Parser Name:** ", logdata.parserinfo["PARSER_NAME"])
                st.write("**Parser Version:** ", logdata.parserinfo["PARSER_VERSION"])
                st.write("**Parser Author:** ", logdata.parserinfo["PARSER_AUTHOR"])
                st.write(
                    "**Parser Description:** ", logdata.parserinfo["PARSER_DESCRIPTION"]
                )


if __name__ == "__main__":
    default_dashboard()
