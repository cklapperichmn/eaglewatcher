"""
This is the main commandline utility for eaglewatcher. It contains 2 functions:
    - run - the main function, can be imported and used separately; ie not as a commandline tool. arguments identical to the commandline args.
    - run_with_click: calls run() and passes the arguments in
"""

import queue
import subprocess
import time
from subprocess import Popen
from typing import List
from eaglewatcher.eagle_loggers import get_loggers
from eaglewatcher.core_functionality import (
    get_event_processing_thread,
    get_event_generator_for_directory,
    DirSnapshot,
)
from eaglewatcher.data_structures import UPDATEMODE
import click
import os
import importlib
import sys
from eaglewatcher.stoppable_threads import register_kill_signals

DEFAULT_PORT = "8501"


@click.command()
@click.option(
    "-d",
    "--target_dir",
    help="The directory to watch for new files. Can use this argument multiple times.",
    default=["../sampledata"],
    multiple=True,
)
@click.option(
    "-m",
    "--update_mode",
    default="single",
    help='The update mode to use. Options are "continuous" or "single". This controls if the program runs once or continuously.',
)
@click.option(
    "-i",
    "--polling_interval",
    default=10,
    help="The number of seconds to wait between polling the target directory for new files",
)
@click.option(
    "-b",
    "--buffer_size",
    default=20,
    help="The number of files to buffer before processing."
    "Modified and created events are buffered separately,"
    "so the total number of files that can be buffered is 2x this number",
)
@click.option(
    "-p",
    "--parser_path",
    default="../sampleparser.py",
    help="The file that contains the parser to use",
)
@click.option(
    "--no_dash",
    is_flag=True,
    help="This flag disables the dashboard launch. Useful for running in a container or on a headless server",
)
@click.option(
    "--resume",
    is_flag=True,
    help="resumes were you left off by reading in {yourparsername}.pkl in the same dir as your parser",
)
@click.option("--port", help="the port for the dashboard", default="8501")
def run_with_click(*args, **kwargs) -> None:
    # Run via commandline. This had to be separated from run() for testing and code re-usability reasons.
    run(*args, **kwargs)


def run(
    target_dir: List[str],
    update_mode: str = UPDATEMODE.SINGLE,
    polling_interval: int = 10,
    buffer_size: int = 20,
    parser_path: str = "../sampleparser.py",
    no_dash=False,
    resume=True,
    port: str = "8501",
) -> None:
    """
    This is the main function that runs the program. It starts the event processing thread, and the event generator threads.
    :param target_dir: The directory to watch for new files. Can use this argument multiple times.
    :param update_mode: The update mode to use. Options are "continuous" or "single". This controls if the program runs once or continuously.
    :param polling_interval: The number of seconds to wait between polling the target directory for new files
    :param buffer_size: The number of files to buffer before processing. Modified and created events are buffered separately,
    so the total number of files that can be buffered is 2x this number
    :param parser_path: The file that contains the fileparser instance to use. see sampleparser.py for an example
    :return: None
    """

    # First we do input validation

    # rename for convenience, since this arg is a list
    target_directories = target_dir

    # validate inputs
    if not type(target_directories) in (list, tuple):
        raise TypeError("TARGET_DIRECTORY must be a list or tuple of strings")
    for target_directory in target_directories:
        if not os.path.exists(target_directory):
            raise FileNotFoundError(
                f"TARGET_DIRECTORY: {target_directory} does not exist"
            )
    for target_directory in target_directories:
        if not os.path.isdir(target_directory):
            raise NotADirectoryError(
                f"TARGET_DIRECTORY: {target_directory} is not a directory"
            )
    if not os.path.isfile(parser_path):
        raise FileNotFoundError(
            f"PARSER_PATH: {parser_path} does not exist or is not a valid .py file"
        )
    # make sure port can be converted to an int
    try:
        int(port)
    except:
        raise ValueError("--port argument must be integer")

    update_mode = UPDATEMODE(update_mode)
    assert update_mode in (UPDATEMODE.SINGLE, UPDATEMODE.CONTINUOUS)
    assert polling_interval > 0
    assert buffer_size > 0

    # Load the parser

    sys.path.append(
        os.path.dirname(parser_path)
    )  # this is to allow the parser to import other files in the same directory
    parser_path = os.path.join(os.getcwd(), parser_path)
    module_name = os.path.basename(parser_path).split(".")[0]
    spec = importlib.util.spec_from_file_location(module_name, parser_path)
    parser_module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = parser_module
    spec.loader.exec_module(parser_module)
    fileparser = parser_module.fileparser

    parserinfo = fileparser.get_parser_info()

    # setup logging
    loggers = get_loggers(parserinfo, target_directories)
    systemlogger, userlogger = loggers["system"], loggers["user"]
    logfilename = loggers["filepath"]
    loggers = {"system": systemlogger, "user": userlogger}

    # convert all directories to absolute paths
    full_target_directories = []
    for d in target_directories:
        if not os.path.isabs(d):
            full_target_directories.append(os.path.join(os.getcwd(), d))
        else:
            full_target_directories.append(d)
    target_directories = full_target_directories

    # Next, ensure no target directories are 'redundant' - fully contained within another
    # this is n^2 but n is small
    remove_these = []
    for target_directory in target_directories:
        for other_target_directory in target_directories:
            if target_directory == other_target_directory:
                continue
            if target_directory in other_target_directory:
                remove_these.append(target_directory)
                loggers["system"].warning(
                    f"TARGET_DIRECTORY: {target_directory} is fully contained within {other_target_directory}. Removing it from the list of target directories"
                )
    target_directories = [x for x in target_directories if x not in remove_these]

    # Start the event processing threads
    eventqueue = queue.SimpleQueue()
    eventprocessor = get_event_processing_thread(
        fileparser, eventqueue, loggers, update_mode, buffer_size
    )
    eventprocessor.start()

    # Read the snapshot
    snapshot_filename = f'{parserinfo["PARSER_NAME"]}.pkl'
    parser_dir = os.path.dirname(parser_path)
    snapshot_path = os.path.join(parser_dir, snapshot_filename)
    snapshot = DirSnapshot(snapshot_path, loggers["system"])
    if resume:
        snapshot.load()

    # Start event generator threads
    event_generators = []
    for target_directory in target_directories:
        eventgenerator = get_event_generator_for_directory(
            eventqueue,
            target_directory,
            fileparser,
            polling_interval,
            update_mode,
            systemlogger,
            snapshot,
        )
        event_generators.append(eventgenerator)
        eventgenerator.start()

    # Start the Dashboard
    p = None
    if not no_dash:
        p = dashboard_launch(logfilename, port=port)

    # Register signals
    register_kill_signals(process_to_kill=p)

    # wait for the threads to quit
    # have to use this instead of join, so we can recieve signals. See this link: https://stackoverflow.com/questions/631441/interruptible-thread-join-in-python
    while eventprocessor.is_alive():
        time.sleep(1)
    # once again, cannot use join
    for eventgenerator in event_generators:
        while eventgenerator.is_alive():
            time.sleep(1)

    if not no_dash:
        p.wait()


def dashboard_launch(logfilename: str, port: str = DEFAULT_PORT) -> subprocess.Popen:
    """
    Launches the dashboard in a separate process
    :param port: The port to launch the dashboard on
    :return: The process object
    """
    dashboard_dir = os.path.dirname(__file__)
    dashboard_path = os.path.join(dashboard_dir, "dash.py")

    # LAUNCH THE DASHBOARD
    # we define the theme via commandline arg to avoid needing to change the current working directory
    # which would screw up a lot of things with our app, it's bad to screw with the users cwd
    # we also define the logdir via commandline arg so the user can specify where the logfiles are
    primaryColor = "#E694FF"
    backgroundColor = "#00172B"
    secondaryBackgroundColor = "#0083B8"
    base = "dark"
    font = "sans serif"
    textcolor = "white"
    # this is necessary because I have not figured out how to get streamlit to read from the correct .streamlit folder
    # both the process cwd() and the dashboard itself can be literally anywhere and there's no way to specify the path to the .streamlit folder
    credentials_dir = os.path.join(
        os.path.expanduser("~"), ".streamlit", "credentials.toml"
    )
    if not os.path.exists(credentials_dir):
        os.makedirs(os.path.dirname(credentials_dir), exist_ok=True)
        with open(credentials_dir, "w+") as f:
            f.write(f'[general]\nemail = ""\n')

    system_command = [
        "streamlit",
        "run",
        dashboard_path,
        "--theme.font",
        font,
        "--theme.textColor",
        textcolor,
        "--theme.secondaryBackgroundColor",
        secondaryBackgroundColor,
        "--theme.backgroundColor",
        backgroundColor,
        "--theme.primaryColor",
        primaryColor,
        "--theme.base",
        base,
        "--server.headless",
        "false",
        "--browser.gatherUsageStats",
        "false",
        "--runner.magicEnabled",
        "false",
        "--",
        "--logfile",
        logfilename,
    ]
    if port != DEFAULT_PORT:
        system_command += ["--server.port", port]
    p = Popen(system_command)
    return p


if __name__ == "__main__":
    run_with_click()
