pydantic>=2.6,<3
pandas>=2.2,<3
pyarrow>=15.0,<16
openpyxl>=3,<4
streamlit>1.0,<2
concurrent_log_handler