"""
NOT MAINTAINED.
Code for testing functionality of watchdog library and for experimenting with watchdog_extensions.
"""
import os
import queue
import shutil
import sys
import threading
import time
import logging

import watchdog.utils.dirsnapshot
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.observers.api import DEFAULT_EMITTER_TIMEOUT, EventEmitter, BaseObserver, DEFAULT_OBSERVER_TIMEOUT
from watchdog.observers.polling import PollingEmitter
from watchdog.utils.dirsnapshot import DirectorySnapshot, EmptyDirectorySnapshot
class PollingObserverWithInitEvents(BaseObserver):
    """
    Platform-independent observer that polls a directory to detect file
    system changes.
    """

    def __init__(self, timeout=DEFAULT_OBSERVER_TIMEOUT):
        super().__init__(emitter_class=InitialPollingEmitter, timeout=timeout)
class InitialPollingEmitter(PollingEmitter):
    """
    Platform-independent emitter that polls a directory to detect file
    system changes, but generates create events at the beginning of the thread.
    """
    def on_thread_start(self):
        self._snapshot = EmptyDirectorySnapshot()

if __name__ == "__main__":
    from watchdog.observers.api import BaseObserver

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    # Set format for displaying path
    path = '../sampledata'

    # Initialize logging event handler
    event_handler = LoggingEventHandler()

    # Initialize Observer
    observer = PollingObserverWithInitEvents()
    observer.schedule(event_handler, path, recursive=True)

    # Start the observer
    observer.start()
    try:
        while True:
            # Set the thread sleep time
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()