# OPTION 1
import time


def route_2():
    import sys
    import streamlit.web.cli as stcli
    sys.argv = ["streamlit", "run", "application/main/services/streamlit_app.py"]
    sys.exit(stcli.main())

def route_1():
    # OPTION 2
    import subprocess
    import os

    process = subprocess.Popen(["streamlit", "run", 'dash.py'])

    return process

import signal
def sighandler(signum, frame):
    print('Signal handler called with signal', signum)
    # p.kill()
    # p.wait()
    # print('done!')

if __name__ == '__main__':
    p = route_1()
    time.sleep(10)

    signal.signal(signal.SIGINT, handler=sighandler)
    # signal.signal(signal.SIGTERM, handler=lambda x,y: p.kill())
    p.wait()
    print('done!')