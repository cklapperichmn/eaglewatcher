"""
Classes:
    DirSnapshot: A class that stores the previous state of a directory. This is used to generate events for new files, modified files, and moved files.

Functions:
    generate_event_for_filepath: Generates events and modifies the snapshot.
    get_retry_func: Returns a function that retries a function a specified number of times.
    process_buffered_data: Processes the buffered data.
    get_event_processing_thread: Returns a thread that processes events from the eventqueue.
    get_event_generator_for_directory: Returns a thread that polls a directory for new files and sends events to a queue.
"""

import os
import pickle
import queue
import time
from typing import Optional, Any, Callable, List, Tuple, Dict, Type
from eaglewatcher.data_structures import (FileEventType, PollingCompleteEvent, ProcessingEventType, LoggingEventType,
                                          get_processingfunc_from_processingevent, UPDATEMODE, FileSystemEvent,
                                          get_processingevent_from_fileevent)
import logging
from eaglewatcher.fileparser import fileparser
from eaglewatcher.stoppable_threads import StoppableThread

NETWORK_RECONNECT_SECONDS = 60

import threading

class DirSnapshot:
    def __init__(self, filename:str=None, logger:Optional[logging.Logger]=None):
        self.lock = threading.Lock()
        self.filename = filename
        self.path_to_statinfo = {}
        self.inode_to_path = {}
        self.skipped_folders = set()
        self.skipped_files = set()

        if not logger:
            logger = logging.getLogger(__name__)
        self.logger = logger

    def load(self)->bool:
        with self.lock:
            try:
                with open(self.filename,'rb') as f:
                    snapshot = pickle.load(f)
                    self.path_to_statinfo = snapshot.get('path_to_statinfo',{})
                    self.inode_to_path = snapshot.get('inode_to_path',{})
                    self.skipped_files = snapshot.get('skipped_files',set())
                    self.skipped_folders = snapshot.get('skipped_folders',set())

                self.logger.info(f'Loaded snapshot from {self.filename}', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':self.filename})
                return True
            except:
                self.logger.exception(f'Failed to load snapshot from {self.filename}', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':self.filename})
                return False
    def save(self)->bool:
        savedata = {
            'path_to_statinfo': self.path_to_statinfo,
            'inode_to_path': self.inode_to_path,
            'skipped_folders': self.skipped_folders,
            'skipped_files': self.skipped_files
        }
        try:
            with self.lock:
                with open(self.filename, 'wb') as f:
                    pickle.dump(savedata, f, protocol=3)
            self.logger.info(f'Saved snapshot to {self.filename}', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':self.filename})
            return True
        except:
            self.logger.exception(f'Failed to save snapshot to {self.filename}', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':self.filename})
            return False

def generate_event_for_filepath(filepath:str, stat_info:os.stat_result, snapshot:DirSnapshot)->Optional[FileSystemEvent]:
    """
    Generates events and modifies the snapshot.
    ! only generates CREATE, MODIFY, and MOVE events. DELETE events are generated after polling the entire directory once and comparing.
    This function takes a filepath, and returns a PollingEvent based on the previous snapshot.
    This runs for every file, not once per polling. This is super slow the first time, but fast subsequently.
    :param filepath: The full path to the file
    :param mtime: The mtime of the file
    :param previous_snapshot: A dictionary of filepaths and their mtimes
    :return: A PollingEvent
    """
    inode = stat_info.st_ino
    prev_filepath = snapshot.inode_to_path.get(stat_info.st_ino)

    if inode not in snapshot.inode_to_path:
        event = FileSystemEvent(src_path=filepath, event_type=FileEventType.CREATE, eventtime=stat_info.st_ctime)
    elif filepath == prev_filepath and stat_info.st_mtime != snapshot.path_to_statinfo.get(filepath).st_mtime:
        event = FileSystemEvent(src_path=filepath, event_type=FileEventType.MODIFY, eventtime=stat_info.st_mtime)
    elif inode in snapshot.inode_to_path and filepath != prev_filepath:
        del snapshot.path_to_statinfo[prev_filepath]
        event = FileSystemEvent(src_path=snapshot.inode_to_path.get(inode), dst_path=filepath, event_type=FileEventType.MOVE, eventtime=stat_info.st_mtime)
    else:
        event = None

    snapshot.inode_to_path[inode] = filepath
    snapshot.path_to_statinfo[filepath] = stat_info
    return event

def get_retry_func(tries:int=2, delay:int=3, backoff:int=2, logger:Optional[logging.Logger]=None, exception_to_check:Exception=Exception)->Callable[[Callable],Callable]:
    """
    :param ExceptionToCheck: The exception to check for
    :param tries: The number of times to try before giving up
    :param delay: The number of seconds to wait before the first retry
    :param backoff: The number of seconds to increase the delay by after each retry
    :return: The result of the function
    """
    def f_retry(f, *args, **kwargs):
        mtries, mdelay = tries, delay
        while mtries > 1:
            try:
                return f(*args, **kwargs)
            except exception_to_check as e:
                if logger:
                    logger.exception(f'Exception: {e}. Retrying in {mdelay} seconds...', extra={'operationname':LoggingEventType.SYSTEM, 'filepath': ''})
                time.sleep(mdelay)
                mtries -= 1
                mdelay *= backoff
        return f(*args, **kwargs)
    return f_retry

def process_buffered_data(filebuffers, parser, file_buffer_size, systemlogger, userlogger, ignore_buffer_limit:bool=False):
    def _handle_buffer(processing_event_type, filebuffers: dict, process_function: Callable, ignore_buffer_limit: bool = False):
        """
        Handles the buffer processing logic for both MODIFIED and CREATED events.
         Both even types get their own separate buffer.
         A buffer is a list of (path, data)
         If the event is an ALL_FILES_PROCESSED event, or if the buffer is full,
         process the buffer, then clear it. On failure, move the files to a failed directory.
         'filebuffers' is a dict which maps an EventType to a list of (str,Any) tuples.
        """
        eventdata = filebuffers.get(processing_event_type)
        if not eventdata:
            return

        filepaths = [x[0] for x in eventdata]
        data = [x[1] for x in eventdata]

        if not filepaths:
            return

        if (ignore_buffer_limit) or (len(filepaths) >= file_buffer_size):
            try:
                process_function(data, filepaths, userlogger)
                for fullpath in filepaths:
                    systemlogger.info(f'{len(data)} files in buffer',
                                      extra={'operationname': processing_event_type, 'filepath': fullpath})
            except Exception as e:
                for fullpath in filepaths:
                    systemlogger.exception(f'{len(data)} files in buffer',
                                           extra={'operationname': processing_event_type, 'filepath': fullpath})
            filebuffers[processing_event_type].clear()

    # if we have reached the buffer size, or if all files have been processed, process the buffer
    for event_type in (ProcessingEventType.PROCESS, ProcessingEventType.UPDATE, ProcessingEventType.DELETE):
        processing_func = get_processingfunc_from_processingevent(event_type, parser)

        _handle_buffer(event_type, filebuffers, processing_func, ignore_buffer_limit=ignore_buffer_limit)

def get_event_processing_thread(parser_class:Type[fileparser], eventqueue:queue.SimpleQueue, loggers:Dict[str,logging.Logger],
                                update_mode: UPDATEMODE, file_buffer_size=20)->threading.Thread:
    """
    This function processes events from the eventqueue, and updates the parser with new data.
    :param parser: a fileparser object from sampleparser.py
    :param eventqueue: a queue of filesystem events to process
    :param loggers: a logger dict containing the keys 'system' and 'user'
    :param update_mode: The update mode to use. Options are "continuous" or "single". This controls if the program runs once or continuously.
    :param file_buffer_size: The number of files to buffer before processing. Modified and created events are buffered separately, so the total number of files that can be buffered is 2x this number
    :return: A thread that processes events from the eventqueue
    """
    systemlogger, userlogger = loggers['system'], loggers['user']

    retry_func = get_retry_func(tries=2, delay=3, backoff=2, logger=None)

    def process_events(stop_event:threading.Event):
        try:
            parser = parser_class()  # initialize the parser. have to initialize here so sqlite3 works
        except:
            name=parser_class.get_parser_info()['PARSER_NAME']
            systemlogger.exception(f'Failed to initialize parser {name}', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':''})
            return
        n_events = 0
        filebuffers = {
            ProcessingEventType.PROCESS: [],
            ProcessingEventType.UPDATE: [],
            ProcessingEventType.DELETE: [],
        }
        while True:
            while True:
                if stop_event.is_set():
                    return
                try:
                    event = eventqueue.get(timeout=1)
                    break
                except queue.Empty:
                    pass

            if type(event) == PollingCompleteEvent:
                # systemlogger.info(f'Polling complete for {event.src_path}. {event.n_files} files processed.', extra={'operationname':LoggingEvent.SYSTEM, 'filepath':event.src_path})
                process_buffered_data(filebuffers, parser, file_buffer_size, systemlogger, userlogger, ignore_buffer_limit=True)
                if update_mode == UPDATEMODE.SINGLE:
                    return
                continue

            n_events += 1

            # Normalize the path to ensure it's in a consistent format
            normalized_path = os.path.normpath(event.src_path)
            # Split the path into its components
            path_parts = normalized_path.split(os.sep)
            # Return the base folder, which is the first component
            basefolder = path_parts[0]

            try:
                is_valid = parser.is_valid_file(event.src_path)
            except Exception as e:
                systemlogger.exception(msg='Failed to validate File.', extra={'operationname':LoggingEventType.SKIP, 'filepath':event.src_path})
                continue

            # whether it was created or modified, the file still needs to be read.
            if event.event_type==FileEventType.DELETE and is_valid:
                filebuffers[ProcessingEventType.DELETE].append((event.src_path, None))
            if (event.event_type in (FileEventType.CREATE, FileEventType.MODIFY)) and is_valid:
                try:
                    data = retry_func(parser.read_file, event.src_path, userlogger)
                    systemlogger.info(f'SUCCESS', extra={'operationname':ProcessingEventType.READ, 'filepath':event.src_path})
                    if data is not None:
                        matching_processing_event = get_processingevent_from_fileevent(event.event_type)
                        filebuffers[matching_processing_event].append((event.src_path, data))
                    else:
                        systemlogger.warning(f'No Data Read', extra={'operationname':ProcessingEventType.READ, 'filepath':event.src_path})
                except Exception as e:
                    systemlogger.exception(e.__str__(), extra={'operationname':ProcessingEventType.READ, 'filepath':event.src_path})

            process_buffered_data(filebuffers, parser, file_buffer_size, systemlogger, userlogger)

    # the function definition has now ended. The function is not yet running.
    return StoppableThread(target=process_events, daemon=False)

def get_event_generator_for_directory(eventqueue: queue.SimpleQueue, target_directory:str,
                                      parser:Type[fileparser], polling_interval:int or float, update_mode: UPDATEMODE,
                                      logger:logging.Logger, snapshot:DirSnapshot=None)->threading.Thread:
    """
    This function returns a thread that polls a directory for new files and sends events to a queue.
    :param eventqueue_list: Events are broadcast to every queue in this list.
    :param target_directory: The directory to watch for new or modified files
    :param parsers: A list of fileparsers to use to process the files, from sampleparser.py
    :param polling_interval: The number of seconds to wait between polls
    :param update_mode: The update mode to use. Options are "continuous" or "single". This controls if the program runs once or continuously.
    :param logger: A logger object from get_logger()
    :param snapshot: Optional, A DirSnapshot object that contains the previous state of the directory
    :return: A thread that polls the directory for new files and sends events to the eventqueue
    """
    snapshot = snapshot or DirSnapshot()
    def poll(stop_event:threading.Event):
        retry_func = get_retry_func(tries=2, delay=3, backoff=2, logger=logger)

        # should these be moved into the DirSnapshot class?
        visited_paths = set() # this is used for tracking delete events

        while True:
            if stop_event.is_set():
                snapshot.save()
                return

            n_events = 0
            while not os.path.exists(target_directory):
                logger.error(f'Directory {target_directory} does not exist. Check network connection.',
                               extra={'operationname': LoggingEventType.SYSTEM, 'filepath': target_directory})
                time.sleep(NETWORK_RECONNECT_SECONDS)

            try:
                for root, dirs, files in os.walk(target_directory, followlinks=False):
                    # verify the folder is valid
                    try:
                        is_valid_folder = parser.is_valid_folder(root)
                    except:
                        is_valid_folder = False
                        logger.exception(f'Failed to validate folder.', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':root})

                    if not is_valid_folder and root not in snapshot.skipped_folders:
                        snapshot.skipped_folders.add(root)
                        logger.info(msg=f'Skipping folder: {root}', extra={'operationname': LoggingEventType.SKIP, 'filepath': root})
                        continue

                    for file in files:
                        fullpath = os.path.join(root, file)
                        if not os.path.exists(fullpath):
                            logger.error(f'File {fullpath} does not exist. Check network connection.',
                                           extra={'operationname':LoggingEventType.SYSTEM, 'filepath':fullpath})
                            continue # skip this file and iteration of the loop if the file doesn't exist

                        # verify the path is valid
                        try:
                            is_valid = parser.is_valid_file(fullpath)
                        except:
                            is_valid = False
                            logger.exception('Failed to validate file', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':fullpath})
                        if not is_valid and fullpath not in snapshot.skipped_files:
                            snapshot.skipped_files.add(fullpath)
                            logger.info(msg=f'Skipping file: {fullpath}', extra={'operationname':LoggingEventType.SKIP, 'filepath':fullpath})
                            continue

                        try: # read the file's mtime, but only on valid paths
                            statinfo:os.stat_result = retry_func(os.stat, fullpath)
                        except Exception as e:
                            logger.exception('Failed to get mtime for file', extra={'operationname':LoggingEventType.SYSTEM, 'filepath':fullpath})
                            continue

                        event = generate_event_for_filepath(fullpath, statinfo, snapshot)
                        visited_paths.add(fullpath)
                        if event:
                            n_events+=1
                            logger.info(msg=f'',extra={'operationname':event.event_type, 'filepath':fullpath})
                            eventqueue.put(event)

                # POLING COMPLETE - CLEANUPDELETE EVENTS
                missing_paths = set(snapshot.path_to_statinfo.keys()) - visited_paths
                for path in missing_paths:
                    event = FileSystemEvent(src_path=path, event_type=FileEventType.DELETE, eventtime=time.time())
                    eventqueue.put(event)
                    logger.info(event, extra={'operationname':FileEventType.DELETE, 'filepath':path})
                    inode = snapshot.path_to_statinfo[path].st_ino
                    del snapshot.inode_to_path[inode]
                    del snapshot.path_to_statinfo[path]

                # POLLING COMPLETE
                visited_paths.clear()
                if n_events > 0:
                    eventqueue.put(PollingCompleteEvent(src_path=target_directory, n_files=n_events, eventtime=time.time()))
                    result = snapshot.save()  # thread-safe!

                if update_mode == UPDATEMODE.SINGLE:
                    return

            except Exception as e:
                logger.exception(msg=f'Error polling directory',
                                 extra={'operationname':LoggingEventType.SYSTEM, 'filepath':target_directory})

            time.sleep(polling_interval)

    return StoppableThread(target=poll, daemon=False)