"""
This file defines the basic interface for the fileparser. This is to allow for typehinting and code completion,
as well as to be the 'contract' that all fileparsers must adhere to
"""

import logging
from typing import List, Any


class fileparser:
    def __init__(self):
        pass

    @staticmethod
    def get_parser_info() -> dict:
        pass

    @staticmethod
    def is_valid_file(fullpath: str) -> bool:
        pass

    def read_file(fullpath: str, logger: logging.Logger) -> List[Any]:
        pass

    def process_new_files(
        self, data: List, filepaths: List[str], logger: logging.Logger
    ) -> None:
        pass

    def update_data(
        self, data: List, filepaths: List[str], logger: logging.Logger
    ) -> None:
        pass

    def delete_data(self, filepaths: List[str], logger: logging.Logger) -> None:
        pass
