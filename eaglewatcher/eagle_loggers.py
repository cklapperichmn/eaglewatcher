"""
This module contains custom logging classes and functions for Eaglewatcher.
It provides thread-safe loggers with rollover/backup, support for logging metadata to the top of a log file,
a filter to keep track of the last logged filepath, and functions to read and summarize logfiles as dataframes.

the basic functions for the end user are:
    1. get_loggers(parserinfo:dict, target_directories: List[str], clear_previous_logfile:bool=False) -> Dict[str, logging.Logger]
    returns a dict of 2 loggers, keys are 'system' and 'users'. The user logger logs to the same file as the system logger,
    but with a different format.
    2. read_logfile(loglines:List[str]) -> LogData
    to get the dataframe, use logdata.dataframe
    LogData structure is described in data_structures.py
    3. summarize_logdata_dataframe(df:pd.DataFrame)->pd.DataFrame
    returns a summarized dataframe with 1 row per filepath: the most recent log entry for that filepath
"""

from datetime import datetime
from concurrent_log_handler import ConcurrentRotatingFileHandler
from typing import List, Dict
import pandas as pd
import os
from eaglewatcher.data_structures import LogData
import logging


def summarize_logdata_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    """
    Takes a dataframe and returns a summarized dataframe with 1 row per filepath: the most recent log entry for that filepath
    :param df: a dataframe with columns: datetime, loglevel, operationtype, status, filepath, message
    :return: a dataframe with 1 row per filepath
    """
    df["datetime"] = pd.to_datetime(df["datetime"], format="%Y-%m-%d %H:%M:%S,%f")
    df = df.sort_values("datetime", ascending=False)
    df = df.groupby("fullpath").first().reset_index()
    return df


def _is_logline(line: str) -> bool:
    """
    Determines if a line is a valid Eaglewatcher log line. May grow more complex as needed, for now this works.
    :param line: the log line, a single line with no newline chars
    :return: True if valid, False if not
    """
    try:
        return len(line.split("|")) == 5
    except:
        return False


def read_logfile(loglines: List[str]) -> LogData:
    """
    This function reads a logfile and returns a LogData object
    each row of the dataframe contains a datetime, loglevel, operationtype, status, filepath, message
    - loglevels are the standard logvels defined in the logging module, PLUS an extra 'TRACEBACK' loglevel
    - operationtype is a string that describes the operation that was performed, defined in datastructures.py
    all PollingEvents, LoggingEvents, and FileEvents are valid operationtypes
    - status is either 'SUCCESS' or 'ERROR'
    - message is a string that describes the event.
    :param loglines: a list of strings, each string is a line from a logfile
    :return: LogData - defined in datastructures.py
    """
    target_directories = []
    last_updated = loglines[-1].split(" | ")[0]
    parserinfo = {}
    rows = []

    current_exception = None
    current_traceback = []
    inside_traceback = False

    # parse exceptions
    for i, line in enumerate(loglines):
        # IF valid logline, attempt to parse the logline.
        is_logline = _is_logline(line)

        # if we are inside a traceback, append the line to the current traceback and begin a traceback tracking process
        if "Traceback" == line[:9] and not inside_traceback:
            if i == 0:
                continue  # this should never happen

            inside_traceback = True
            current_exception = loglines[i - 1]  # previous line
            current_traceback = []
            current_traceback.append(line)
            continue

        if not is_logline and inside_traceback:
            current_traceback.append(line)
            continue

        if (is_logline or (i == len(loglines) - 1)) and inside_traceback:
            inside_traceback = False
            row = current_exception
            row = row.split("|")
            row = [x.strip() for x in row]
            row[2] = "TRACEBACK"
            row[4] = "\n".join(current_traceback)
            # columns=['datetime', 'loglevel', 'operationtype', 'status', 'filepath', 'message'])
            row = [row[0], row[1], row[2], "ERROR", row[3], row[4]]
            rows.append(row)
            current_exception = None

    for line in loglines:
        try:
            fields = line.split("|")
            fields = [x.strip() for x in fields]
            datetime, loglevel, operationtype, filepath, message = (
                fields[0],
                fields[1],
                fields[2],
                fields[3],
                "|".join(fields[4:]),
            )
        except IndexError as e:
            continue

        filepath = filepath.split("PATH:")[-1].strip()
        message = message.strip()

        if loglevel in ("NOTSET", "INFO", "WARNING", "DEBUG"):
            status = "SUCCESS"
        elif loglevel in ("ERROR", "CRITICAL"):
            status = "ERROR"
        else:
            status = loglevel

        if operationtype == "METADATA":
            # target directories
            if "TARGET_DIRECTORY" in line:
                target_directory = message.split("TARGET_DIRECTORY:")[-1].strip()
                target_directories.append(target_directory)

            # parser info
            elif "PARSER" in line:
                key, value = message.split(":")
                parserinfo[key.strip()] = value.strip()
            else:
                logging.error(f"Unknown METADATA line: {line}")

        else:
            # columns = ['datetime', 'loglevel', 'operationtype', 'status', 'filepath', 'message'])
            rows.append([datetime, loglevel, operationtype, status, filepath, message])

    df = pd.DataFrame(
        rows,
        columns=[
            "datetime",
            "loglevel",
            "operationtype",
            "status",
            "filepath",
            "message",
        ],
    )
    paths = list(df[df["filepath"] != "NA"]["filepath"].unique())
    try:
        commonpath = os.path.commonpath(paths)
    except:
        commonpath = ""

    df["fullpath"] = df["filepath"]
    df["filepath"] = df["filepath"].str.replace(commonpath, "")
    logdata = LogData(
        last_updated=last_updated,
        commonpath=commonpath,
        target_directories=target_directories,
        dataframe=df,
        parserinfo=parserinfo,
    )
    return logdata


class _ConcurrentRotatingFileHandlerWithHeaders(ConcurrentRotatingFileHandler):
    """
    Custom rotating file handler that can add header messages to the log file and is threadsafe.
    """

    def __init__(self, filename, mode="a", maxBytes=0, backupCount=0, encoding=None):
        super().__init__(filename, mode, maxBytes, backupCount, encoding)
        self.header_messages = []  # List to store system messages

    def add_header_message(self, message):
        """Add a system message to the list of headers."""
        self.header_messages.append(message)

    def doRollover(self):
        """Do a rollover, as described in __init__."""
        super().doRollover()  # Call the parent class's method to perform the actual rollover
        # After rolling over, add the header messages to the new log file
        if self.header_messages:
            _write_log_with_metadata(
                self.baseFilename, self.header_messages
            )  # Rewrite the log file with the header messages


def _write_log_with_metadata(logfilename: str, new_metadata: List[str]) -> None:
    """
    reads in a log and rewrites the entire thing with the metadata, stripping out any existing metadata lines as necessary.
    """
    with open(logfilename, "w") as f:
        for line in new_metadata:
            f.write(line + "\n")


class _FilepathTrackingFilter(logging.Filter):
    """
    This class tracks the previous filepath, and ensures the user can log without specifying a filepath.
    """

    def __init__(self):
        super().__init__()
        self.last_filepath = "NA"

    def filter(self, record):
        if hasattr(record, "filepath"):
            # Update the last known filepath if it's not 'NA'
            if record.filepath != "NA":
                self.last_filepath = record.filepath
        else:
            record.filepath = (
                self.last_filepath
            )  # Set the record's filepath to the last known filepath
        return True  # Return True to indicate that logging should proceed


def get_loggers(
    parserinfo: dict,
    target_directories: List[str],
    max_log_size_kb: int = 4096,
    clear_previous_logfile: bool = True,
    target_logdir=None,
) -> Dict[str, logging.Logger]:
    """
    This function returns a logger that logs to the console and to a file.
    LOGFORMAT
    TARGET_DIRECTORY: /path/to/directory1
    TARGET_DIRECTORY: /path/to/directory2
    TARGET_DIRECTORY: /path/to/directory3
    PARSER_NAME: parser_name | PARSER_VERSION: 1.0.0 | PARSER_AUTHOR: author_name | PARSER_DESCRIPTION: parser_description
    2024-02-26 12:49:37,584 | LOGLEVEL | PARSER_NAME | OPERATIONTYPE | MESSAGE | PATH:FULLPATH |
    2024-02-26 12:49:37,584 | LOGLEVEL | PARSER_NAME | OPERATIONTYPE | MESSAGE | PATH:FULLPATH |
    2024-02-26 12:49:37,584 | LOGLEVEL | PARSER_NAME | OPERATIONTYPE | MESSAGE | PATH:FULLPATH |
    :return: a tuple of loggers: 1 system logger and 1 user logger
    """
    parsername = parserinfo["PARSER_NAME"]
    year_month_day_hour_minute_second = "%Y-%m-%d-%H-%M-%S"
    datestring = datetime.now().strftime(year_month_day_hour_minute_second)
    logname = f'{parsername}-{parserinfo["PARSER_VERSION"]}'  # -{datestring}'

    if not target_logdir:
        target_logdir = os.path.join(os.getcwd(), f"{logname}_logs")
        os.makedirs(target_logdir, exist_ok=True)
    logfilename = os.path.join(target_logdir, f"{logname}.log")

    if clear_previous_logfile:
        with open(logfilename, "w") as f:
            f.write("")

    # 2 loggers, and 2 log formats. For the user, we want to always set user, and default the path to NA.
    userlogformat = (
        f"%(asctime)s | %(levelname)s | USER | PATH:%(filepath)s | %(message)s"
    )
    system_logformat = f"%(asctime)s | %(levelname)s | %(operationname)s | PATH:%(filepath)s | %(message)s"
    logger = logging.getLogger(logname)

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(system_logformat))
    logger.addHandler(console)

    filehandler = _ConcurrentRotatingFileHandlerWithHeaders(
        logfilename, "a+", max_log_size_kb * 1024, 2
    )
    filehandler.setFormatter(logging.Formatter(system_logformat))
    logger.addHandler(filehandler)
    logger.setLevel(logging.INFO)

    user_logger = logging.getLogger(f"{parsername}_User")
    userfilehandler = _ConcurrentRotatingFileHandlerWithHeaders(
        logfilename, "a", max_log_size_kb * 1024, 2
    )
    userfilehandler.setFormatter(logging.Formatter(userlogformat))
    user_logger.addHandler(userfilehandler)
    user_logger.setLevel(logging.INFO)
    user_logger.addFilter(_FilepathTrackingFilter())

    # LOG SYSTEM INFO / LOGGING METADATA
    metadata_header_messages = []
    for target_directory in target_directories:
        metadata_header_messages.append(
            system_logformat
            % {
                "asctime": "NA",
                "levelname": "INFO",
                "operationname": "METADATA",
                "filepath": "NA",
                "message": f"TARGET_DIRECTORY: {target_directory}",
            }
        )

    for key, value in parserinfo.items():
        metadata_header_messages.append(
            system_logformat
            % {
                "asctime": "NA",
                "levelname": "INFO",
                "operationname": "METADATA",
                "filepath": "NA",
                "message": f"{key}: {value}",
            }
        )

    # Need to initialize the logfile with the metadata
    _write_log_with_metadata(logfilename, metadata_header_messages)

    for filehandler in [filehandler, userfilehandler]:
        if metadata_header_messages:
            for message in metadata_header_messages:
                filehandler.add_header_message(message)

    loggers = {"system": logger, "user": user_logger, "filepath": logfilename}
    return loggers
