import shutil
import sys
import os
import traceback

import eaglewatcher

sys.path.append(os.path.dirname(__file__))
from eaglewatcher import data_structures
import queue
import time
import unittest
import pandas as pd
from eaglewatcher import eagle_loggers
import sampleparser
from eaglewatcher.core_functionality import (
    get_event_processing_thread,
    get_event_generator_for_directory,
)
import logging
from eaglewatcher.eagle_loggers import get_loggers


def get_dummy_logger():
    logger = logging.getLogger("dummy")
    logger.setLevel(logging.DEBUG)
    return logger


def generate_fake_data(directory):
    for i in range(5):
        with open(os.path.join(directory, f"test{i}.csv"), "w") as f:
            df = pd.DataFrame({"a": [1 * i, 2 * i, 3 * i]})
            df.to_csv(f, index=False)


class TestEagleWatcher(unittest.TestCase):
    test_dir = "sampledata"

    def setUp(self):
        if "tests.py" in os.listdir():
            os.chdir("..")
        if not os.path.exists(self.test_dir):
            os.makedirs(self.test_dir)
        generate_fake_data(self.test_dir)

    def test_Event(self):
        event = data_structures.FileSystemEvent(
            src_path="test.csv",
            event_type=data_structures.FileEventType.CREATE,
            eventtime=1628784000,
        )
        self.assertEqual(event.src_path, "test.csv")
        self.assertEqual(event.event_type, data_structures.FileEventType.CREATE)

        event = data_structures.FileSystemEvent(
            src_path="test.csv", event_type=data_structures.FileEventType.CREATE
        )
        self.assertEqual(event.eventtime, 0)

    def test_deleted_moved_events(self):
        parser = sampleparser.fileparser
        logger = get_loggers(parser.get_parser_info(), [self.test_dir])["system"]
        eventqueue = queue.SimpleQueue()
        target_directory = self.test_dir
        polling_interval = 0.3
        updatemode = data_structures.UPDATEMODE.CONTINUOUS
        thread = get_event_generator_for_directory(
            eventqueue, target_directory, parser, polling_interval, updatemode, logger
        )
        self.assertTrue(thread)
        thread.start()
        time.sleep(2)
        shutil.move(
            os.path.join(self.test_dir, "test0.csv"),
            os.path.join(self.test_dir, "test0_copy.csv"),
        )
        time.sleep(2)
        os.remove(os.path.join(self.test_dir, "test0_copy.csv"))
        time.sleep(2)
        self.assertTrue(thread.is_alive())
        self.assertFalse(eventqueue.empty())

        events = []
        while not eventqueue.empty():
            events.append(eventqueue.get())

        events_movedelete = [
            x
            for x in events
            if "FileSystemEvent" in str(type(x))
            and str(x.event_type)
            in (
                str(data_structures.FileEventType.MOVE),
                str(data_structures.FileEventType.DELETE),
            )
        ]

        self.assertTrue(len(events_movedelete) == 2)

    def test_get_loggers(self):
        parser = sampleparser.fileparser()
        parserinfo = parser.get_parser_info()
        loggers = eagle_loggers.get_loggers(parserinfo, [self.test_dir])
        systemlogger, userlogger = loggers["system"], loggers["user"]
        self.assertTrue(systemlogger)
        self.assertTrue(userlogger)
        time.sleep(1)

        logname = f'{parserinfo["PARSER_NAME"]}-{parserinfo["PARSER_VERSION"]}.log'

        fullpath = r"a/b/c/d/e/f/g/test.csv"
        systemlogger.info(
            msg="TEST", extra={"operationname": "SYSTEM", "filepath": fullpath}
        )
        try:
            raise ValueError("this is a test exception.")
        except Exception as e:
            systemlogger.exception(
                "TEST EXCEPTION",
                extra={"operationname": "EXCEPTION", "filepath": fullpath},
            )
        systemlogger.info(
            msg="TEST", extra={"operationname": "SYSTEM", "filepath": fullpath}
        )
        userlogger.info(msg="TEST")

        with open(logname, "r") as f:
            self.assertTrue("TEST" in f.read())
        time.sleep(1)

    def test_logdata_to_dataframe(self):
        with open("sampledata/test_logfile.log", "r") as f:
            loglines = f.readlines()

        logdata = eagle_loggers.read_logfile(loglines)

        self.assertTrue(logdata.parserinfo.get("PARSER_NAME"))
        logdata_df = logdata.dataframe

        self.assertTrue(isinstance(logdata_df, pd.DataFrame))
        self.assertTrue("fullpath" in logdata_df.columns)
        self.assertTrue("datetime" in logdata_df.columns)
        self.assertTrue("operationtype" in logdata_df.columns)
        self.assertTrue("status" in logdata_df.columns)
        self.assertTrue("message" in logdata_df.columns)
        operationtypes = logdata_df["operationtype"].unique()
        try:
            assert set(operationtypes).issubset(
                {
                    "TRACEBACK",
                    "USER",
                    "SYSTEM",
                    "SKIP",
                    "MODIFY",
                    "MOVE",
                    "CREATE",
                    "SKIP",
                    "DELETE",
                    "READ",
                    "PROCESS",
                    "UPDATE",
                }
            )
        except:
            print(set(operationtypes))
        status = logdata_df["status"].unique()
        try:
            assert set(status) == {"ERROR", "SUCCESS"}
        except:
            print(set(status))

        loglevels = logdata_df["loglevel"].unique()

        # assert everything in loglevels are in the standard python logging loglevels
        loglevels_from_dataframe = set(logdata_df["loglevel"].unique())
        standard_loglevels = set([logging.getLevelName(x) for x in range(0, 100)])
        assert loglevels_from_dataframe.issubset(standard_loglevels)

        tracebacks = logdata_df[logdata_df["operationtype"] == "TRACEBACK"]
        assert len(tracebacks) > 0
        summarize_df = eagle_loggers.summarize_logdata_dataframe(logdata_df)
        self.assertTrue(isinstance(summarize_df, pd.DataFrame))

        # print(logdata)
        # print(tracebacks['message'].values[0])
        # print(summarize_df)

    def test_resume_eventgenerator(self):
        parser = sampleparser.fileparser
        logger = get_loggers(parser.get_parser_info(), [self.test_dir])["system"]
        eventqueue = queue.SimpleQueue()
        target_directory = self.test_dir
        polling_interval = 0.3
        updatemode = data_structures.UPDATEMODE.CONTINUOUS
        thread = get_event_generator_for_directory(
            eventqueue, target_directory, parser, polling_interval, updatemode, logger
        )

        thread.start()
        time.sleep(2)

    def tearDown(self):
        pass

    def test_get_event_processing_thread(self):
        import queue

        eventqueue = queue.SimpleQueue()

        updatemode = data_structures.UPDATEMODE.CONTINUOUS
        buffersize = 20
        parser = sampleparser.fileparser
        loggers = get_loggers(parser.get_parser_info(), [self.test_dir])
        thread = get_event_processing_thread(
            parser, eventqueue, loggers, updatemode, buffersize
        )
        self.assertTrue(thread)
        thread.start()

        for file in os.listdir(self.test_dir):
            event = eaglewatcher.data_structures.FileSystemEvent(
                src_path=os.path.join(self.test_dir, file),
                event_type=data_structures.FileEventType.CREATE,
                eventtime=0,
            )
            eventqueue.put(event)
        finalevent = data_structures.PollingCompleteEvent(
            src_path=self.test_dir, n_files=5, eventtime=0
        )
        eventqueue.put(finalevent)

        time.sleep(4)
        self.assertTrue(thread.is_alive())
        if not eventqueue.empty():
            print(eventqueue.get())
        self.assertTrue(eventqueue.empty())
        self.assertTrue(
            os.path.exists(os.path.join("sampleoutputs", "all_data.feather"))
        )

    def test_get_event_generator_for_directory(self):
        loggers = get_loggers(
            sampleparser.fileparser().get_parser_info(), [self.test_dir]
        )
        systemlogger, userlogger = loggers["system"], loggers["user"]
        eventqueue = queue.SimpleQueue()
        target_directory = self.test_dir
        polling_interval = 2
        updatemode = data_structures.UPDATEMODE.CONTINUOUS
        parser = sampleparser.fileparser
        thread = get_event_generator_for_directory(
            eventqueue,
            target_directory,
            parser,
            polling_interval,
            updatemode,
            systemlogger,
        )
        self.assertTrue(thread)
        thread.start()
        time.sleep(2)

        self.assertTrue(thread.is_alive())
        n = 0
        while not eventqueue.empty():
            event = eventqueue.get()
            print(n, event)
            if "PollingCompleteEvent" in str(type(event)):
                self.assertTrue(eventqueue.empty())
            elif "FileSystemEvent" in str(type(event)):
                self.assertTrue(".csv" in event.src_path)
            else:
                print(event)
                self.assertTrue(False)

            n += 1

    def test_logger_rollover(self):
        logger = get_loggers(
            sampleparser.fileparser().get_parser_info(),
            [self.test_dir],
            max_log_size_kb=1,
        )["system"]
        for i in range(100):
            logger.info(
                "TEST", extra={"operationname": "SYSTEM", "filepath": "test.csv"}
            )

        # verify the rollover triggered and that the metadata is present.
        for log in ("sampleparser-1.0.0.log", "sampleparser-1.0.0.log.1"):
            print(log)
            with open(log, "r") as f:
                lines = f.readlines()
            self.assertTrue(len(lines) > 0)
            self.assertTrue("METADATA" in lines[0])

    def test_run(self):
        import threading, eaglewatcher

        target_dir = [self.test_dir]
        update_mode = "single"
        polling_interval = 2
        buffer_size = 20
        from eaglewatcher import command_line

        t = threading.Thread(
            target=command_line.run,
            kwargs={
                "target_dir": target_dir,
                "update_mode": update_mode,
                "polling_interval": polling_interval,
                "buffer_size": buffer_size,
                "parser_path": "sampleparser.py",
                "no_dash": True,
            },
        )
        t.start()
        time.sleep(5)
        self.assertTrue(os.path.exists("sampleoutputs/all_data.feather"))
        self.assertTrue(os.path.exists("sampleparser-1.0.0.log"))


class TestUserFunctions(unittest.TestCase):
    test_dir = "../sampledata"

    def setUp(self):
        if not os.path.exists(self.test_dir):
            os.makedirs(self.test_dir)
        generate_fake_data(self.test_dir)

    def tearDown(self):
        pass

    def test_is_valid_file(self):
        reader = sampleparser.fileparser
        self.assertTrue(reader.is_valid_file("test.csv"))
        self.assertTrue(reader.is_valid_file("test.CSV"))
        self.assertFalse(reader.is_valid_file("test.txt"))
        self.assertFalse(reader.is_valid_file("test"))

    def test_read_file(self):
        logger = get_dummy_logger()
        reader = sampleparser.fileparser()
        df = reader.read_file(os.path.join(self.test_dir, "test0.csv"), logger)
        reference_df = pd.DataFrame({"a": [0, 0, 0]})
        print(df)
        self.assertTrue(pd.DataFrame.equals(df["a"], reference_df["a"]))


if __name__ == "__main__":
    unittest.main()
