# Create a setup.py project for this file below:

from setuptools import setup, find_packages

setup(
    name="eaglewatcher",
    version="0.13",
    packages=find_packages(
        exclude=[
            "sampledata",
            "sampleoutputs",
            "projects",
            "concatenator",
            ".venv",
            "__pycache__",
        ]
    ),
    install_requires=[
        "pandas",
        "plotly",
        "streamlit",
        "dash",
        "concurrent_log_handler",
        "streamlit",
        "openpyxl",
        "pyarrow",
        "pydantic",
    ],
    entry_points={
        "console_scripts": [
            "eaglewatcher = eaglewatcher.command_line:run_with_click",
        ]
    },
)
