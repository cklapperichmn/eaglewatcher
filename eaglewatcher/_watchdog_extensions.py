"""
NOT MAINTAINED
Contains extensions to the watchdog library. Would be used to replace Eaglewatchers file event handling system.
Currently eaglewatcher uses a custom filewatching and eventhandling system.
 but the limitations and difficulties of adapting Watchdog to Eaglewatcher's needs make it likely not worth it.
"""

import logging
import os
import threading
from functools import partial
from typing import Tuple, Optional, Callable, Iterator

from eaglewatcher.data_structures import UPDATEMODE, FileEventType
from watchdog.events import FileSystemEventHandler
from watchdog.observers.api import BaseObserver, DEFAULT_OBSERVER_TIMEOUT
from watchdog.observers.polling import PollingEmitter
from watchdog.utils.dirsnapshot import EmptyDirectorySnapshot, DirectorySnapshot
# we have these 2 things that generate events. what we need is a special deduplication queue that can track events.
# to avoid adding events that have already been added.
class SharedDirectorySnapshot(DirectorySnapshot):

    def __init__(
        self,
        path: str,
        recursive: bool = True,
        stat: Callable[[str], os.stat_result] = os.stat,
        listdir: Callable[[Optional[str]], Iterator[os.DirEntry]] = os.scandir,
    ):
        self.recursive = recursive
        self.stat = stat
        self.listdir = listdir
        self.lock = threading.Lock()
        self._stat_info: dict[str, os.stat_result] = {}
        self._inode_to_path: dict[Tuple[int, int], str] = {}

        st = self.stat(path)
        self._stat_info[path] = st
        self._inode_to_path[(st.st_ino, st.st_dev)] = path

        for p, st in self.walk(path):
            i = (st.st_ino, st.st_dev)
            with self.lock:
                self._inode_to_path[i] = p
                self._stat_info[p] = st


class PollingObserverWithInitEvents(BaseObserver):
    """
    Platform-independent observer that polls a directory to detect file
    system changes.
    """

    def __init__(self, timeout=DEFAULT_OBSERVER_TIMEOUT, update_mode=UPDATEMODE.CONTINUOUS):
        super().__init__(emitter_class=partial(InitialPollingEmitter,directory_snapshot=SharedDirectorySnapshot, update_mode=update_mode), timeout=timeout)
class InitialPollingEmitter(PollingEmitter):
    """
    Platform-independent emitter that polls a directory to detect file
    system changes, but generates create events at the beginning of the thread.
    """

    def __init__(self,
                 directory_snapshot,
                 update_mode,
                 # this needs to be a threadsafe, shared object
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        self._update_mode=update_mode
        self._snapshot = directory_snapshot
    def on_thread_start(self):
        self._snapshot = EmptyDirectorySnapshot()

class CallableFilterEventHandler(FileSystemEventHandler):
    def __init__(self, filter_function):
        """
        Initializes the handler with a custom filter function.
        The filter function should take an event as its argument and return True if the event should be processed.
        """
        self.filter_function = filter_function

    def on_any_event(self, event):
        """Override to filter events based on the provided callable."""
        if self.filter_function(event):
            super().on_any_event(event)

class EaglewatcherEventHandler(FileSystemEventHandler):
    def __init__(self, filter_function, logger):
        """
        Initializes the handler with a custom filter function.
        The filter function should take an event as its argument and return True if the event should be processed.
        """
        self.logger = logger
        self.filter_function = filter_function

    def on_any_event(self, event):
        """Override to filter events based on the provided callable."""
        if self.filter_function(event):
            super().on_any_event(event)
    def on_created(self, event):
        self.logger.info(f'Created: {event.src_path}', extra={'operationname':FileEventType.CREATE, 'filepath':event.src_path})
        super().on_created(event)

def event_generator_thread_watchdog(target_directory:str,
                                    parser, polling_interval:int or float, update_mode: UPDATEMODE,
                                    logger:logging.Logger)->threading.Thread:
    from watchdog_extensions import PollingObserverWithInitEvents, CallableFilterEventHandler

    observer = PollingObserverWithInitEvents(polling_interval)
    event_handler = EaglewatcherEventHandler(filter_function=parser.is_valid_file, logger=logger)

    observer.schedule(event_handler, target_directory, recursive=True)

    return threading.Thread(target=observer.start(), daemon=True)