"""
MODIFY THIS FILE with your code for reading, processing, and writing data!
Rewrite the following functions:
__init__()
is_valid_file
read_file
process_new_files

OPTIONAL:
is_valid_folder
update_data
delete_data

Don't modify anything else.
error handling and logging is handled for you!

USER LOGGING:
Each function has a 'logger' passed in, if you want to do additional logging.
When using the logger, you can log the filepath by using 'extra={'filepath': f}'
This makes your logs more compatible with the dashboard
You can set log levels with logger.info logging.warning etc, and the dashboard will pick up on that.

modify the 'if __name__=='__main__':' block and run this file to test your parser.
"""

import os
import logging
from typing import Any, List
from functools import lru_cache
import pandas as pd
import sqlite3


class fileparser:
    def __init__(self):
        # This function is called once when the program starts.
        # Use it to initialize variables, create directories, etc

        # create a directory to store the outputs
        self.output_dir = "sampleoutputs"
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        # initialize a dataframe
        self.alldata = pd.DataFrame()

        # LOCAL DATABASE CONNECTION
        self.database_path = os.path.join(self.output_dir, "sampledata.db")
        self.conn = sqlite3.connect(self.database_path)
        self.c = self.conn.cursor()
        self.tablename = "sampledata"
        self.c.execute(
            f"""CREATE TABLE IF NOT EXISTS {self.tablename} (a int, b int, c int, filepath str)"""
        )
        self.conn.commit()

    @staticmethod
    def get_parser_info() -> dict:
        # replace this with your information, if you want
        infoblock = {
            "PARSER_NAME": "sampleparser",
            "PARSER_VERSION": "1.0.0",
            "PARSER_AUTHOR": "klappec",
            "PARSER_DESCRIPTION": "This is a sample parser. It reads csv files and stores them in a .csv, .feather, and sqlite3 .db file.",
        }
        return infoblock

    @staticmethod
    @lru_cache(maxsize=None)
    def is_valid_file(fullpath: str) -> bool:
        """
        Make this return false when you want to skip a file, and return True otherwise.
        """
        is_valid = fullpath.lower().endswith(".csv")
        return is_valid

    @staticmethod
    @lru_cache(maxsize=None)
    def is_valid_folder(folderpath: str) -> bool:
        """
        Make this return False when you want to skip a folder, and return True otherwise.
        """
        return True

    def read_file(self, fullpath: str, logger: logging.Logger) -> Any:
        # Read your data, then return your data.
        # No rules, any format you want. Try and be consistent about the return type.
        df = pd.read_csv(fullpath)
        df["filepath"] = fullpath
        return df

    def process_new_files(
        self, data: List, filepaths: List[str], logger: logging.Logger
    ) -> None:
        # 'data' is a list of things returned from read_data() function that you wrote.
        # 'filepaths' is a list of filepaths that were read.
        # use this function to process and write your data
        # when using the logger, you can log the filepath by using 'extra={'filepath': f}'
        # this makes your logs compatible with the dashboard

        # EXAMPLE LOGGING - replace this and do whatever you want
        for f in filepaths:
            logger.info(f"EXAMPLE LOG MESSAGE", extra={"filepath": f})

        self.alldata = pd.concat([self.alldata] + data)
        self.alldata.to_feather(os.path.join(self.output_dir, "all_data.feather"))
        self.alldata.to_csv(os.path.join(self.output_dir, "all_data.csv"))

        for df in data:
            df.to_sql(self.tablename, self.conn, if_exists="append", index=False)
        self.conn.commit()

    def update_data(
        self, data: List, filepaths: List[str], logger: logging.Logger
    ) -> None:
        # This processes files that have been MODIFIED since the program started.
        # You've likely already processed them once with process_new_files.
        # Most use cases will just write 'pass' here.
        self.alldata = self.alldata.iloc[
            self.alldata["filepath"].isin(filepaths), :
        ] = pd.concat(data)

    def delete_data(self, filepaths: List[str], logger: logging.Logger) -> None:
        # This processes files that have been DELETED since the program started.
        # You've likely already processed them once with process_new_files.
        # Most use cases will just write 'pass' here.
        self.alldata = self.alldata[~self.alldata["filepath"].isin(filepaths)]


# ===================================================================
# DEBUG / TEST MODE FOR MAKING SURE YOUR PARSER IS WORKING CORRECTLY
# ===================================================================
if __name__ == "__main__":
    # MODIFY THESE
    target_dir = "sampledata"
    max_n = 3

    # =================================================================================================
    # The below code is what the entirety of eaglewatcher could have been, if I wasn't a crazy person.
    # =================================================================================================
    n = 0
    buffer = []
    filepaths = []
    parser = fileparser()
    logger = logging.getLogger()
    logger.info("test", extra={"filepath": "test"})
    for path, folders, files in os.walk(target_dir, followlinks=False):
        if not fileparser.is_valid_folder(path):
            logger.info(f"INVALID FOLDER: skipping {path}")
            continue
        for f in files:
            fullpath = os.path.join(path, f)
            if not fileparser.is_valid_file(fullpath):
                logger.info(f"INVALID FILE: skipping {fullpath}")
                continue
            logger.info(fullpath)
            filepaths.append(fullpath)
            data, metadata = parser.read_file(fullpath, logging.getLogger())
            buffer.append((data, metadata))
            n += 1
            if n == max_n:
                break
        if n == max_n:
            break
    parser.process_new_files(buffer, filepaths, logger)
