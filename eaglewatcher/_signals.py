import signal
import threading
import time
import streamlit as st

def test_process():
    while True:
        print('running test process.')
        time.sleep(1)

def test_stoppable_thread(flag):
    print(flag)
    while not flag.is_set():
        print('running stoppable thread')
        time.sleep(1)
    print('done running stoppable thread. Joining.')

from eaglewatcher.stoppable_threads import StoppableThread

if __name__ == '__main__':
    import subprocess
    process = subprocess.Popen('streamlit run sample_dashboard.py', shell=True)

    t = StoppableThread(target=test_stoppable_thread, daemon=False)
    t.start()

    def signal_handler(sig, frame):
        print('You pressed Ctrl+C!')
        # raise KeyboardInterrupt
        print('Eaglewatcher is stopping all threads. Please wait...')
        for thread in threading.enumerate():
            if isinstance(thread, StoppableThread):
                thread.stop()
        if process:
            process.terminate()
            print("Process Terminated.")
        print('DONE WITH SIGNAL HANDLER')

    signal.signal(signal.SIGINT, signal_handler)

    print('Press Ctrl+C')
    while t.is_alive():
        time.sleep(1)
    process.wait()
    print('exiting.')